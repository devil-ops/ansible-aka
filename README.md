# Overview

Use ansible to create or modify records in AKA

## Setup

Make sure you have an AKA API token

Set up a couple environment variables so that Ansible can auth against aka:

```
export AKA_USERNAME=your_user
export AKA_TOKEN=your_token
```

## Usage:

#### aka_a_record

This is for creating, deleting and modifying A records.  Example usage:

```yaml
    - name: Test that my module works
      aka_a_record:
        name: "drew-test-ansible.oit.duke.edu"
        address: "152.3.102.213"
        ttl: 100
        state: present
      register: result

    - debug: var=result
```
