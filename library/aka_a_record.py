#!/usr/bin/env python3

from ansible.module_utils.basic import AnsibleModule
import requests
import logging
import os
import sys


def a_record_present(data=None):

    aka_server = os.environ.get('AKA_ADDRESS', 'aka.oit.duke.edu')
    username = os.environ.get('AKA_USERNAME')
    token = os.environ.get('AKA_TOKEN')

    if not username or not token:
        logging.error("Make sure AKA_USERNAME and AKA_TOKEN are set\n")
        sys.exit(2)

    existing_records = requests.get("https://%s/api/v1/dns/%s" %
                                    (aka_server, data['name']),
                                    auth=(username, token)).json()
    target_record = None
    for existing_record in existing_records:
        if existing_record['view'] == data['view']:
            target_record = existing_record

    if not target_record:
        logging.debug("Creating a brand new record")
        sys.stderr.write("%s\n" % target_record)
        return False, True, target_record
    else:
        logging.debug("Updating existing record")

        # Ok we have a record, lets see if it needs to change
        needs_change = False
        if data['ttl'] != target_record['ttl']:
            logging.debug("Needs change because of ttl mismatch")
            needs_change = True
        if data['address'] != target_record['address']:
            logging.debug("Needs change because of address mismatch")
            needs_change = True

        if needs_change:
            body = {
                'ttl': data['ttl'],
                'view': data['view'],
                'address': data['address']
            }
            update = requests.put(
                "https://%s/api/v1/dns/%s/A/%s" %
                (aka_server, data['name'], target_record['address']),
                auth=(username, token),
                json=body).json()

            if "errors" in update:
                return True, True, update["errors"]
            else:
                return False, True, update
        else:
            return False, False, target_record


def a_record_absent(data=None):
    return 0


def main():

    fields = {
        "name": {
            "required": True,
            "type": "str"
        },
        "address": {
            "required": True,
            "type": "str"
        },
        "state": {
            "default": "present",
            "choices": ['present', 'absent'],
            "type": 'str'
        },
        "view": {
            "default": "Internal",
            "choices": ['Internal', 'External'],
            "type": 'str'
        },
        "ttl": {
            "type": int,
            "default": 600
        }
    }

    choice_map = {
        "present": a_record_present,
        "absent": a_record_absent,
    }

    module = AnsibleModule(argument_spec=fields)
    # module.exit_json(changed=False, meta=a_record_present(module.params))
    is_error, has_changed, result = choice_map.get(module.params['state'])(
        module.params)

    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="Error changing record", meta=result)


if __name__ == '__main__':
    main()
